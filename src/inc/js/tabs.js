var Tabs = {
  init: function () {
    this.bindUIFunctions();
    this.pageLoadCorrectTab();
  },
  bindUIFunctions: function () {
    $(document)
      .on('click', '.tab__nav:not(.tab__nav--active)', function (e) {
        e.preventDefault();
        Tabs.changeTab(this.hash);
      });
  },
  pageLoadCorrectTab: function () {
    this.changeTab(document.location.hash);
  },
  changeTab: function (hash) {
    var anchor = $('[href="' + hash + '"]');
    var div = $(hash);

    anchor.addClass('tab__nav--active').siblings().removeClass('tab__nav--active');
    div.addClass('tab__content--active').siblings().removeClass('tab__content--active');

  }
};

Tabs.init();