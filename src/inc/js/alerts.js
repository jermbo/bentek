var Alerts = (function() {

  var $wrapper = $('#alerts-wrapper');

  events.on('success', renderSuccess);
  events.on('message', renderInfo);
  events.on('info', renderInfo);
  events.on('warning', renderWarning);
  events.on('danger', renderDanger);
  events.on('fail', renderDanger);
  events.on('error', renderDanger);

  function renderSuccess(data) {
    addDom('success', data);
  }

  function renderInfo(data) {
    addDom('info', data);
  }

  function renderWarning(data) {
    addDom('warning', data);
  }

  function renderDanger(data) {
    addDom('danger', data);
  }

  function addDom(type, msg) {
    var $alert = $('<div/>').addClass('alert alert--' + type);
    var innerHTML = '<div class="alert__inner">';
    innerHTML += '<p class="alert__message">' + msg + '</p>';
    innerHTML += '<a href="#" class="alert__close">x</a>';
    innerHTML += '</div>';

    $alert.delegate('.alert__close', 'click', deleteModule);
    $alert.append(innerHTML);
    $wrapper.prepend($alert);

    //setTimeout(function() {
    //  $alert.find('.alert__close').trigger('click');
    //}, 3000);
  }

  function deleteModule(evt) {
    evt.preventDefault();
    var $elem = $(evt.target).parents('.alert');
    $elem.animate({
      overflow : 'hidden',
      padding  : '0 15px',
      margin   : 0,
      height   : 0,
      opacity  : 0
    }, function() {
      $(this).remove();
    });
  }

})();