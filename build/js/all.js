var events = {
  events : {},
  on     : function(eventName, fn) {
    this.events[eventName] = this.events[eventName] || [];
    this.events[eventName].push(fn);
  },
  off    : function(eventName, fn) {
    if(this.events[eventName]) {
      for(var i = 0; i < this.events[eventName].length; i++) {
        if(this.events[eventName][i] === fn) {
          this.events[eventName].splice(i, 1);
          break;
        }
      }
    }
  },
  emit   : function(eventName, data) {
    if(this.events[eventName]) {
      this.events[eventName].forEach(function(fn) {
        fn(data);
      });
    }
  }
};
var Alerts = (function() {

  var $wrapper = $('#alerts-wrapper');

  events.on('success', renderSuccess);
  events.on('message', renderInfo);
  events.on('info', renderInfo);
  events.on('warning', renderWarning);
  events.on('danger', renderDanger);
  events.on('fail', renderDanger);
  events.on('error', renderDanger);

  function renderSuccess(data) {
    addDom('success', data);
  }

  function renderInfo(data) {
    addDom('info', data);
  }

  function renderWarning(data) {
    addDom('warning', data);
  }

  function renderDanger(data) {
    addDom('danger', data);
  }

  function addDom(type, msg) {
    var $alert = $('<div/>').addClass('alert alert--' + type);
    var innerHTML = '<div class="alert__inner">';
    innerHTML += '<p class="alert__message">' + msg + '</p>';
    innerHTML += '<a href="#" class="alert__close">x</a>';
    innerHTML += '</div>';

    $alert.delegate('.alert__close', 'click', deleteModule);
    $alert.append(innerHTML);
    $wrapper.prepend($alert);

    //setTimeout(function() {
    //  $alert.find('.alert__close').trigger('click');
    //}, 3000);
  }

  function deleteModule(evt) {
    evt.preventDefault();
    var $elem = $(evt.target).parents('.alert');
    $elem.animate({
      overflow : 'hidden',
      padding  : '0 15px',
      margin   : 0,
      height   : 0,
      opacity  : 0
    }, function() {
      $(this).remove();
    });
  }

})();
function isTouchDevice() {
  //return typeof (window.ontouchstart !== 'undefined') || ();
  return (('ontouchstart' in window) || (navigator.MaxTouchPoints > 0) || (navigator.msMaxTouchPoints > 0));
}


$(function () {

  //$('.date-picker').pikaday({}).format('L');

  $('.date-picker').on('click focus', function (e) {
    e.preventDefault();
    var $this = $(this);
    $this.pikaday({
      onSelect: function (date) {
        $this.val(moment(date).format('L'));
      }
    });
  });


  //var picker = new Pikaday({
  //  field: $('.date-picker')[0],
  //  format: 'Y',
  //  onSelect: function() {
  //
  //    //console.log(this.getMoment().format('Do MMMM YYYY'));
  //    console.log(moment(this._d).format('L'));
  //    //$(this).val(moment(this._d).format('L'));
  //    console.log(this);
  //  }
  //});


  /*$('.date-picker').ionDatePicker({
   format: 'MM/DD/YYYY',
   years: 150,
   onClick: function (date) {
   console.log(date);
   }
   });*/

  $('.form__item select').selectric({
    maxHeight: 200,
    arrowButtonMarkup: '<b class="arrow">&#x25be;</b>',
    responsive: true
  });

  $('.nav__item').find('a').on('click', function (e) {
    if ($(this).attr('href') == '#') e.preventDefault();
    $(this).siblings('.sub-nav').stop().slideToggle();
  });

  var $mainNav = $('.main-nav');

  var navTL = new TimelineMax();
  navTL
    .to($mainNav, 0.75, {left: '0', ease: Strong.easeOut}, 'start')
    .staggerFrom('.nav__item', 0.5, {left: '-50%', autoAlpha: 0, ease: Back.easeOut}, 0.13, 'start')
    .reverse();

  $('.nav-btn').on('click', function (e) {
    e.preventDefault();
    navTL.reversed(!navTL.reversed());
  });

  var $body = $('body');
  $body.on('click', '.accordion-menu', function () {
    var $this = $(this),
      $parent = $this.parents('.accordion-group'),
      $content = $parent.find('.accordion-content'),
      $arrow = $parent.find('.accordion__info i'),
      aniTime = 0.25;

    if (!$parent.hasClass('isClosed')) {
      $content.css({'overflow': 'hidden'});
      TweenMax.to($content, aniTime, {height: 0});
      TweenMax.to($arrow, aniTime, {rotation: 0});
      $parent.addClass('isClosed');
    } else {
      TweenMax.set($content, {height: 'auto'});
      TweenMax.from($content, aniTime, {height: 0, onComplete: isHidden, onCompleteParams: [$content, 'visible']});
      TweenMax.to($arrow, aniTime, {rotation: 90});
      $parent.removeClass('isClosed');
    }
  });

  $body.on('click', '.section__header--trigger', function () {
    var $this = $(this),
      $parent = $this.parents('.section'),
      $content = $parent.find('.section__content'),
      $arrow = $parent.find('.section__arrow i'),
      aniTime = 0.25;

    if (!$parent.hasClass('isClosed')) {
      $content.css({'overflow': 'hidden'});
      TweenMax.to($content, aniTime, {height: 0});
      TweenMax.to($arrow, aniTime, {rotation: 0, left: 0});
      $parent.addClass('isClosed');
    } else {
      TweenMax.set($content, {height: 'auto'});
      TweenMax.from($content, aniTime, {height: 0, onComplete: isHidden, onCompleteParams: [$content, 'visible']});
      TweenMax.to($arrow, aniTime, {rotation: 90, left: -2});
      $parent.removeClass('isClosed');
    }
  });


  function isHidden(elem, isHidden) {
    $(elem).css({'overflow': isHidden});
  }

  $('#loginForm').validetta({
    realTime: true,
    errorTemplateClass: 'validetta-inline',
    onValid: function (e) {
      e.preventDefault();
      window.location.href = '/landing-page.html';
    }
    //remote : {
    //  validator_name : {
    //    // Here, you must use ajax setting determined by jQuery
    //    // More info : http://api.jquery.com/jquery.ajax/#jQuery-ajax-settings
    //    type : 'POST',
    //    url : '#',
    //    datatype : 'json'
    //  }
    //}
  });

  $('.slider--range').slider({
    range: 'min',
    min: 0,
    max: 100,
    value: 50,
    slide: function (e, ui) {
      return $('.ui-slider-handle').html(ui.value);
    }
  });

  ///////
  // Shows and Hides help sections
  ///////
  $('.help-nav__item').on('click', function (e) {
    e.preventDefault();
    var id = $(this).attr('href').substr(1);
    $('.help-section').each(function () {
      var $this = $(this);
      $this.addClass('help-hide');
      if ($this.hasClass(id)) {
        $this.removeClass('help-hide');
      }
    });
  });


  /////
  // Make the Progress bar sticky.
  //
  // First, check to see if it exists on page.
  /////
  var $progressArea = $('.progress');
  if ($progressArea.length != 0) {
    var $progressAreaTop = $progressArea.offset().top;

    $(window).on('scroll', function () {
      if ($(this).scrollTop() > $progressAreaTop) {
        $progressArea.addClass('sticky');
      } else {
        $progressArea.removeClass('sticky');
      }
    });
  }

  console.log('hello');
  var $subpull = $('.pull'),
    $submenu = $('.side-nav-wrapper');

  $subpull.on('click', function (e) {
    e.preventDefault();
    $submenu.slideToggle();
  });

  if ($(window).width() < 758) {
    $subpull.trigger('click');
  }


  var $tabBtn = $('.search-tab');

  $tabBtn.on('click', function (e) {
    e.preventDefault();

    var $this = $(this),
      target = $this.attr('data-target'),
      $target = $('#' + target),
      $sibs = $target.siblings('div'),
      aniTime = 0.25;

    if ($target.hasClass('isOpen')) {
      $this.removeClass('search-tab--active');
      $target.removeClass('isOpen');
      TweenMax.to($target, aniTime, {height: 0});
    } else {
      TweenMax.set($target, {height: 'auto'});
      TweenMax.from($target, aniTime, {height: 0, onComplete: tabsActive, onCompleteParams: [$target]});
      TweenMax.to($sibs, aniTime, {height: 0});
      $this.siblings('a').removeClass('search-tab--active');
      $this.addClass('search-tab--active');
      $sibs.removeClass('isOpen');
    }

  });

  function tabsActive(target) {
      target.addClass('isOpen');
  }

  ///////////////////////////////////////////
  /////// PLACE HOLDERS TO BE DELETED ///////
  ///////////////////////////////////////////

  $body.on('click', '.icon--delete', function (e) {
    e.preventDefault();
    var $this = $(this).parents('.section'),
      name = $this.find('.section__title').text();

    swal({
        title: 'Wait!',
        text: 'Are you sure you want to delete ' + name + ' from dependants?',
        type: 'warning',
        closeOnConfirm: true,
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!"
      },
      function () {
        $this.remove();
        events.emit('success', 'You have deleted a dependent');
      })
  });

  $body.on('click', '.icon--edit', function (e) {
    e.preventDefault();
    events.emit('warning', 'You are about to edit a dependant.');
    var $this = $(this).parents('.section');

    $this.addClass('isEditing');
    $this.find('section__content');

    if ($this.hasClass('isClosed')) {
      $this.find('.section__header--trigger').trigger('click');
    }

  });


  $('#addNewDependent').on('click', function (e) {
    e.preventDefault();
    var newGroup = $('.hiddenForm').find('.section').clone();
    console.log(newGroup);
    $('#dependantSection').append(newGroup);
    events.emit('success', 'New Dependent Added!');
  });

});



var Tabs = {
  init: function () {
    this.bindUIFunctions();
    this.pageLoadCorrectTab();
  },
  bindUIFunctions: function () {
    $(document)
      .on('click', '.tab__nav:not(.tab__nav--active)', function (e) {
        e.preventDefault();
        Tabs.changeTab(this.hash);
      });
  },
  pageLoadCorrectTab: function () {
    this.changeTab(document.location.hash);
  },
  changeTab: function (hash) {
    var anchor = $('[href="' + hash + '"]');
    var div = $(hash);

    anchor.addClass('tab__nav--active').siblings().removeClass('tab__nav--active');
    div.addClass('tab__content--active').siblings().removeClass('tab__content--active');

  }
};

Tabs.init();
//# sourceMappingURL=all.js.map
