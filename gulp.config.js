module.exports = function () {

  var source = './src';
  var build = './build';
  var styleGuide = './_styleguide';


  var config = {
    source: source,
    build: build,
    styleGuide: styleGuide,

    fonts: './bower_components/font-awesome/fonts/**/*.*',

    images : source + '/inc/images/**/*.*',

    sass: source + '/inc/sass/**/*.scss',
    guide: styleGuide + '/styles/**/*.scss',
    js: [
      source + '/inc/js/events.js',
      source + '/inc/js/**/*.js'
    ],
    html: source + '/**/*.html',
    partials: source + '/partials/**/*.htm',

    pluginOpts: {
      pattern: ['gulp-*', 'gulp.*', 'main-bower-files'],
      replaceString: /\bgulp[\-.]/
    },
    bower: {
      json: require('./bower.json'),
      directory: './bower_components/',
      ignorePath: '..'
    }

  };

  config.getWiredepOptions = function () {

    return {
      bowerJson: config.bower.json,
      directory: config.bower.directory,
      ignorePath: config.bower.ignorePath
    }
  };


  return config;
};