var gulp = require('gulp');
var browserSync = require('browser-sync');
var config = require('./gulp.config')();
var $ = require('gulp-load-plugins')(config.pluginOpts);

gulp.task('help', $.taskListing);
gulp.task('default', ['help']);

gulp.task('vendor', ['vendorJS', 'vendorCSS']);
gulp.task('startDev', ['styles', 'inject', 'images', 'watch', 'browserSync']);

gulp.task('concatJS', function () {

  return gulp
    .src(config.js)
    .pipe(customPlumber())
    .pipe($.sourcemaps.init())
    .pipe($.concat('all.js'))
    .pipe($.sourcemaps.write('./'))
    .pipe(gulp.dest(config.build + '/js'))
    ;

});

gulp.task('vendorJS', function () {

  return gulp
    .src($.mainBowerFiles())
    .pipe($.filter('**/*.js'))
    .pipe($.concat('vendor.min.js'))
    //.pipe($.uglify())
    .pipe(gulp.dest(config.build + '/js/vendors'))
    ;

});

gulp.task('vendorCSS', function () {

  return gulp
    .src($.mainBowerFiles())
    .pipe($.filter('*.css'))
    .pipe($.concat('vendor.min.css'))
    //.pipe($.uglify())
    .pipe(gulp.dest(config.build + '/styles/vendors'))
    ;

});

gulp.task('styles', function () {

  return gulp.src(config.sass)
    .pipe(customPlumber())
    .pipe($.sourcemaps.init())
    //.pipe($.sass({outputStyle: 'compressed'}))
    .pipe($.sass())
    .pipe($.autoprefixer({browsers: 'last 25 versions'}))
    .pipe($.sourcemaps.write('./'))
    .pipe(gulp.dest(config.build + '/styles'))
    ;
});

gulp.task('fonts', function () {

  return gulp
    .src(config.fonts)
    .pipe(gulp.dest(config.build + '/fonts'))
    ;

});

gulp.task('images', function () {

  return gulp
    .src(config.images)
    .pipe(gulp.dest(config.build + '/images'))
    ;

});


gulp.task('inject', function () {

  var wiredepOptions = config.getWiredepOptions();
  var wiredep = require('wiredep').stream;

  return gulp
    .src(config.html)
    .pipe($.fileInclude({
      prefix: '@@',
      file: '@file'
    }))
    .pipe(wiredep(wiredepOptions))
    .pipe(gulp.dest(config.build))
    ;

});

gulp.task('watch', ['browserSync'], function () {

  gulp.watch(config.sass, ['styles', 'bsReload']);
  gulp.watch([config.html, config.partials], ['inject', 'bsReload']);
  gulp.watch(config.js, ['concatJS', 'bsReload']);

});

gulp.task('browserSync', function () {

  var options = {
    proxy: 'bentek.dev',
    files: [
      config.build + '/styles/**/*.css',
      config.build + '/js/**/*.js',
      config.build + '/*.html'
    ],
    ghostMode: {
      clicks: true,
      location: true,
      forms: true,
      scroll: true
    },
    injectChanges: true,
    notify: true,
    reloadDelay: 0 //1000
  };

  browserSync.init(null, options);

});

gulp.task('bsReload', ['inject'], function () {

  browserSync.reload();

});

gulp.task('somethingHere', $.shell.task([
  'say -v Whisper "It has been done"'
]));


////////// FUNCTIONS
function customPlumber() {
  return $.plumber({
    errorHandler: function (err) {
      console.log(err.stack);
      this.emit('end');
    }
  })
}