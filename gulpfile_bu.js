var gulp = require('gulp');
var browserSync = require('browser-sync');
var config = require('./gulp.config')();
var ftp = require('vinyl-ftp');
var plumber = require('gulp-plumber');
var fileInclude = require('gulp-file-include');

var $ = require('gulp-load-plugins')({lazy: true});


gulp.task('inject', function () {
  gulp.src(config.htmlSrc)
    .pipe(fileInclude({
      prefix: '@@',
      file: '@file'
    }))
    .pipe(gulp.dest(config.allDest))
  ;
});

gulp.task('default', ['styles', 'inject', 'styleGuide', 'watch', 'browserSync']);

gulp.task('styles', function () {
  return gulp.src(config.sassSrc)
    .pipe(customPlumber())
    .pipe($.sourcemaps.init())
    .pipe($.sass({outputStyle: 'compressed'}))
    .pipe($.autoprefixer({browsers: 'last 25 versions'}))
    .pipe($.sourcemaps.write('./'))
    .pipe(gulp.dest(config.allDest + '/styles'))
    ;
});

gulp.task('styleGuide', function () {
  return gulp.src(config.guideSrc)
    .pipe(customPlumber())
    .pipe($.sourcemaps.init())
    .pipe($.sass({outputStyle: 'compressed'}))
    .pipe($.autoprefixer({browsers: 'last 25 versions'}))
    .pipe($.sourcemaps.write('./'))
    .pipe(gulp.dest('./_styleguide/'))
    ;
});

function customPlumber() {
  return plumber({
    errorHandler: function (err) {
      console.log(err.stack);
      this.emit('end');
    }
  })
}

gulp.task('watch', ['browserSync'], function () {
  gulp.watch(config.sassSrc, ['styles', 'bsReload']);
  gulp.watch(config.guideSrc, ['styleGuide', 'bsReload']);
  gulp.watch(config.htmlSrc, ['inject', 'bsReload']);
  gulp.watch(config.htmlPartials, ['inject', 'bsReload']);
  //gulp.watch( config.jsSrc, [ 'scripts', 'bsReload' ] );
  gulp.watch(config.jsSrc, ['bsReload']);
});

gulp.task('bsReload', function () {
  browserSync.reload();
});

gulp.task('browserSync', function () {

  var options = {
    proxy: 'bentek.dev',
    files: [
      config.allDest + 'styles/**/*.css',
      config.allDest + '/js/**/*.js',
      config.src + '/**/*.html',
      '/_styleguide/**/*.css'
    ],
    ghostMode: {
      clicks: true,
      location: true,
      forms: true,
      scroll: true
    },
    injectChanges: true,
    notify: true,
    reloadDelay: 0 //1000
  };

  browserSync.init(null, options);
});


gulp.task('upload', function () {

  var conn = ftp.create({
    host: 'ftp.cosmicstrawberry.com',
    user: 'test@cosmicstrawberry.com',
    pass: '!!Password123',
    parallel: 10,
    log: $.util.log
  });

  //var conn = ftp.create({
  //  host: 'ftp2.mybentek.com',
  //  user: 'jermbo',
  //  pass: 'C0smic$trawb3rry',
  //  port: '22',
  //  parallel: 10,
  //  log: $.util.log
  //});

  var globs = [
    '_styleguide/**/*',
    'inc/**/*',
    'styles/**/*',
    'index.php',
    'gulpfile.js',
    'package.json'
  ];

  return gulp.src(globs, {base: '.', buffer: false})
    .pipe(conn.newer('/'))
    .pipe(conn.dest('/'))
    ;
});